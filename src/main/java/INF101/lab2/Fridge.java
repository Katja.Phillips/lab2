package INF101.lab2;
import java.util.ArrayList;
import java.util.List;

import java.util.NoSuchElementException;

public class Fridge implements IFridge {

    private final int frigdeCapacity = 20;
    private List<FridgeItem> itemsList;

    public Fridge() { // Constructor same name as class
        itemsList = new ArrayList<FridgeItem>();
    } 

    @Override
    public int nItemsInFridge() {
        // Returns the number of element in Fridge, i.e., the length of items (List)
        return itemsList.size();
    }

    @Override
    public int totalSize() {
        // Returns the fridge capacity for an empty fridge
        return frigdeCapacity;
    }

    @Override
    public boolean placeIn(FridgeItem item) { 
        // Tries to place one item in fridge
        if (nItemsInFridge()<totalSize()) {
            itemsList.add(item);
            return true;
        }
        else {return false;
        }
    }

    @Override
    public void takeOut(FridgeItem item) {
        if (itemsList.contains(item)) {
            itemsList.remove(item);
        }
        else{
            throw new NoSuchElementException();
        }
    }

    @Override
    public void emptyFridge() {
        itemsList.clear();
    }

    @Override
    public List<FridgeItem> removeExpiredFood() {
        List<FridgeItem> throwAwayItemsList = new ArrayList<FridgeItem>();
        for (FridgeItem item : this.itemsList) {
            if (item.hasExpired()) {
                throwAwayItemsList.add(item);
            }
        }
        this.itemsList.removeAll(throwAwayItemsList); 
        return throwAwayItemsList;
    }
    
    // private List<FridgeItem> throwAwayItemsList;
    // @Override
    // public List<FridgeItem> removeExpiredFood() {
    //         for (FridgeItem item : this.itemsList) {
    //         if (item.hasExpired()) {
    //             this.takeOut(item);
    //         }
    //     }
    //     return itemsList;
    // }
    
}
